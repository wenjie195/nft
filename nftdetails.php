
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="NFT Details | The Laureate League" />
        <title>NFT Details | The Laureate League</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding  ow-sakura-height">

            <div class="left-nft-img">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic1.png" >
                  </div>
              </div>
			</div>

              <div class="right-nft-div">



                <p class="width100 nft-title-name">Elephant#001</p>
                <table class="nft-table">
                	<tr>
                    	<td class="left-td1">Price</td>
                        <td class="left-td2 price-td">2.5ETH</td>
                    </tr>                
                	<tr>
                    	<td class="left-td1">Owned by</td>
                        <td class="left-td2"><a class="hover-effect red-link font-400">Ele</a></td>
                    </tr>
                	<tr>
                    	<td class="left-td1">Created by</td>
                        <td class="left-td2"><a class="hover-effect red-link font-400">Ele</a></td>
                    </tr>                    
                	<tr>
                    	<td class="left-td1">Collections</td>
                        <td class="left-td2"><a class="hover-effect red-link font-400">Elephant</a></td>
                    </tr>                     
                	<tr>
                    	<td class="left-td1">Blockchain</td>
                        <td class="left-td2">ETH</td>
                    </tr>                       
                	<tr>
                    	<td class="left-td1">Contract Address</td>
                        <td class="left-td2"><a class="hover-effect red-link font-400">0xff9c***13d7</a></td>
                    </tr>   
                	<tr>
                    	<td class="left-td1">Token ID</td>
                        <td class="left-td2"><input type="text" value="91321981255833606897973224722031697500117164304167139448040199898921204449281" id="tokenID" class="id-input">
<button class="copy-btn cursor" onclick="copyID()"><p class="copy-id">91321981255833606897973224722031697500117164304167139448040199898921204449281</p></button></td>
                    </tr>  
                    <tr>
                    	<td class="left-td1">Token Standard</td>
                        <td class="left-td2">ERC-1155</td>
                    </tr>                     
                    <tr>
                    	<td class="left-td1">Metadata</td>
                        <td class="left-td2">Editable</td>
                    </tr>                    
                                      
                </table>

                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <p class="right-fav text-overflow hover1"><span>10</span> <img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></p>
              </div>
            

</div>


<?php include 'js.php'; ?>


</body>
</html>