
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="NFT Market Place | The Laureate League" />
        <title>NFT Market Place | The Laureate League</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="clear"></div>
<div class="width100 overflow menu-distance-margin same-padding index-banner">
	<div class="banner-text-width">
        <h1 class="white-text banner-h1 wow fadeIn" data-wow-delay="0.3s">The Laureate League</h1>
        <p class="banner-p1 white-text wow fadeIn" data-wow-delay="0.6s">is the world’s most trusted and passionate NFT marketplace for everyone to
    purchase, sell, auction and discover attracting NFTs.</p>
        <a href="nft.php"><div class="dual-button collect-css hover-effect wow fadeIn" data-wow-delay="0.9s">Collect</div></a>
        <a href="#"><div class="dual-button create-css hover-effect wow fadeIn" data-wow-delay="1.2s">Create</div></a>
    </div>
    <img src="img/nft.png" class="nft-png">
</div>
<div class="width100 overflow same-padding">
	<h1 class="black-text index-title wow fadeIn" data-wow-delay="0.2s">Latest NFT in the Marketplace</h1>
    <div class="width10x  index4">
    		<!-- Repeat this -->
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect wow fadeIn" data-wow-delay="0.4s">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic1.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Elephant#001</p>
                <p class="width100 grey-desc text-overflow">Ele</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>
			<!-- End of Repeat, can remove bottom -->		
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect wow fadeIn" data-wow-delay="0.6s">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic2.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Elephant#001</p>
                <p class="width100 grey-desc text-overflow">Ele</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>
       
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect wow fadeIn" data-wow-delay="0.8s">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic8.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Deer#002</p>
                <p class="width100 grey-desc text-overflow">Deer</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>           
      
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect wow fadeIn" data-wow-delay="1s">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic4.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Horse#001</p>
                <p class="width100 grey-desc text-overflow">Rider</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>              
	</div>
    <div class="width100 text-center wow fadeIn" data-wow-delay="0.2s">
        <a href="nft.php"><div class="red-btn white-text center-button-size">Browse All</div></a>
    </div>    
    <div class="width100 overflow same-padding padding-bottom50">
        <h1 class="black-text index-title wow fadeIn" data-wow-delay="0.4s">Create and Sell NFTs in Minutes</h1>    
        <div class="clear"></div>
        <div class="same4-div">
        	<img src="img/icon1.png" class="same4-png wow fadeIn" data-wow-delay="0.6s">
            <p class="same4-title wow fadeIn" data-wow-delay="0.8s">Sign Up for an Account</p>
            <p class="same4-p wow fadeIn" data-wow-delay="1.0s">Fill in your creator profile once you’ve signed up for an account.</p>
        </div>
        <div class="same4-div">
        	<img src="img/icon2.png" class="same4-png wow fadeIn" data-wow-delay="1.2s">
            <p class="same4-title wow fadeIn" data-wow-delay="1.4s">Create Your Collection</p>
            <p class="same4-p wow fadeIn" data-wow-delay="1.6s">Click My Collections and set up your collection. Add social links, a description, profile & banner images, and set a secondary sales fee.</p>
        </div>        
        <div class="same4-div">
        	<img src="img/icon3.png" class="same4-png wow fadeIn" data-wow-delay="1.8s">
            <p class="same4-title wow fadeIn" data-wow-delay="2s">Add Your NFTs</p>
            <p class="same4-p wow fadeIn" data-wow-delay="2.2s">Upload your work (image, video, audio, or 3D art), add a title and description, and customize your NFTs with properties, stats, and unlockable content.</p>
        </div>
        <div class="same4-div">
        	<img src="img/icon4.png" class="same4-png wow fadeIn" data-wow-delay="2.4s">
            <p class="same4-title wow fadeIn" data-wow-delay="2.6s">List Them for Sale</p>
            <p class="same4-p wow fadeIn" data-wow-delay="2.8s">Choose between auctions, fixed-price listings, and declining-price listings. You choose how you want to sell your NFTs, and we help you sell them!</p>
        </div>          
        
        
     </div>   
</div>

<?php include 'js.php'; ?>


</body>
</html>