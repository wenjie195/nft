<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://vidatechft.com/hosting-picture/fb-meta-nft.jpg" />
<meta name="author" content="The Laureate League">

<meta property="og:description" content="The Laureate League is the world’s most trusted and passionate NFT marketplace for everyone to purchase, sell, auction and disThe Laureate League is the world’s most trusted and passionate NFT marketplace for everyone to purchase, sell, auction and discover attracting NFTs." />
<meta name="keywords" content="NFT, The Laureate League, TLL, NFTs, blockchain, art, etc">
<!-- Global site tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178461364-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-178461364-1');
</script>-->