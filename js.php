
<div class="footer-div">
	<p class="footer-p wow fadeIn" data-wow-delay="0.1s">&copy;<span id="year"></span> The Laureate League, All Rights Reserved.</p>
</div>
<!-- Login Modal -->
<div id="login-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-login">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 red-text">Login</h1>	   
   <div class="big-white-div">
		<div class="login-div">
        
            <div class="login-input-div">
                <p class="input-top-text">Username</p>
                <input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
            </div>   

            <button class="clean red-btn" >Login</button>
            <p class="text-center margin-bottom0"><a class="open-forgot red-link font-400 forgot-size">Forgot Password?</a></p>
            <p class="text-center margin-bottom0"><a class="open-signup red-link font-400 forgot-size">Register Account</a></p>
         
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-forgot">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 red-text">Forgot Password</h1>	   
   <div class="big-white-div">
		<div class="login-div">
       
            <div class="login-input-div">
                <p class="input-top-text">Email</p>
                <input class="clean tele-input"  type="email" placeholder="Email" name="email" id="email" required>
            </div>  
             

            <button class="clean red-btn">Submit</button>
            <p class="text-center margin-bottom0"><a class="open-login red-link font-400 forgot-size">Login</a></p>
            <p class="text-center margin-bottom0"><a class="open-signup red-link font-400 forgot-size">Register Account</a></p>
        
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Sign Up Modal -->
<div id="signup-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-signup">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 red-text">Sign Up</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <!-- <form action="utilities/adminStaffAddNewFunction.php" method="POST"> -->
         
            <div class="login-input-div">
                <p class="input-top-text">Username</p><!-- cannot repeat with other user-->
                <input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Email</p><!-- cannot repeat with other user-->
                <input class="clean tele-input" type="email" placeholder="Email" id="email" name="email" required>
            </div>             
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
            </div>   
            <div class="login-input-div">
                <p class="input-top-text">Retype Password</p>
                <input class="clean tele-input" type="password" placeholder="Retype Password" id="retype_password" name="retype_password" required>
            </div> 
            <div class="login-input-div">
                <p class="input-top-text">Fullname</p><!-- CAN repeat with other user-->
                <input class="clean tele-input" type="text" placeholder="Fullname" id="fullname" name="fullname" required>
            </div>    
            <div class="login-input-div">
                <p class="input-top-text">Phone No.</p><!-- cannot repeat with other user-->
                <input class="clean tele-input" type="text" placeholder="Phone No." id="phone" name="phone" required>
            </div>                       
            <div class="login-input-div">
                <p class="input-top-text">Country</p>
                <select class="clean tele-input"  id="country" name="country" required>
                	<option>Malaysia</option>
                    <option>Thailand</option>
                </select>
            </div>               
            
            
            <button class="clean red-btn" >Register</button>
            <p class="text-center margin-bottom0"><a class="open-login red-link font-400 forgot-size">Login</a></p>
        
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Filter Modal -->
<div id="filter-modal" class="modal-css">
	<div class="modal-content-css filter-modal-content">
        <span class="close-css close-filter">&times;</span>
        <div class="clear"></div>
   <h1 class="sortby-h1 darkpink-text login-h1 red-text">Sort By</h1>	   
   <div class="big-white-div">
		<div class="login-div">
       
            <div class="filter-option-div">
                <p class="filter-top-text">Time</p>
                <input class="checkbox-budget" type="radio"  id="time1" value="time1" name="timez">
                <label class="for-checkbox-budget" for="time1">Latest</label>
                <input class="checkbox-budget" type="radio"  id="time2" value="time2" name="timez">
                <label class="for-checkbox-budget" for="time2">Oldest</label>       
                <input class="checkbox-budget" type="radio"  id="time3" value="time3" name="timez">
                <label class="for-checkbox-budget" for="time3"> Show Highest Price First</label>      
                <input class="checkbox-budget" type="radio"  id="time4" value="time4" name="timez">
                <label class="for-checkbox-budget" for="time4"> Show Lowest Price First</label>                
                <input class="checkbox-budget" type="radio"  id="time5" value="time5" name="timez">
                <label class="for-checkbox-budget" for="time5"> Most Saved</label>                                        
            </div>  
            <div class="filter-option-div">
                <p class="filter-top-text">Sell in</p>
                <input class="checkbox-budget" type="radio"  id="item1" value="item1" name="itemz">
                <label class="for-checkbox-budget" for="item1">Single Item</label>
                <input class="checkbox-budget" type="radio"  id="item2" value="item2" name="itemz">
                <label class="for-checkbox-budget" for="item2">Bundle</label>                
            </div>              
            <div class="filter-option-div">
                <p class="filter-top-text">Type</p>
                <input class="checkbox-budget" type="radio"  id="type1" value="type1" name="typez">
                <label class="for-checkbox-budget" for="type1">Sale</label>
                <input class="checkbox-budget" type="radio"  id="type2" value="type2" name="typez">
                <label class="for-checkbox-budget" for="type2">Auction</label>                
            </div> 
            <div class="filter-option-div">
                <p class="filter-top-text">Category</p>
                <input class="checkbox-budget" type="radio"  id="category1" value="category1" name="categoryz">
                <label class="for-checkbox-budget" for="category1">Visual Art</label>
                <input class="checkbox-budget" type="radio"  id="category2" value="category2" name="categoryz">
                <label class="for-checkbox-budget" for="category2">Music</label>    
                <input class="checkbox-budget" type="radio"  id="category3" value="category3" name="categoryz">
                <label class="for-checkbox-budget" for="category3">Video</label>                  
                <input class="checkbox-budget" type="radio"  id="category4" value="category4" name="categoryz">
                <label class="for-checkbox-budget" for="category4">Domain Name</label>                              
            </div>             
            <div class="filter-option-div">
                <p class="filter-top-text">Price Range</p>            
            	<select class="filter-select clean">
                	<option>ETH</option>
                    <option>USD</option>
                </select>
				<div class="clear"></div>
                <p class="price-range-p"><input class="filter-price-range clean" type="text">&nbsp;to&nbsp;<input class="filter-price-range clean" type="text">            
            </div>
            <button class="clean red-btn close-filter">Filter Result</button>
            
        
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Filter Modal -->
<div id="filterc-modal" class="modal-css">
	<div class="modal-content-css filter-modal-content">
        <span class="close-css close-filterc">&times;</span>
        <div class="clear"></div>
   <h1 class="sortby-h1 darkpink-text login-h1 red-text">Sort By</h1>	   
   <div class="big-white-div">
		<div class="login-div">
       
            <div class="filter-option-div">
                <p class="filter-top-text">Time</p>
                <input class="checkbox-budget" type="radio"  id="time1" value="time1" name="timez">
                <label class="for-checkbox-budget" for="time1">New</label>
                <input class="checkbox-budget" type="radio"  id="time2" value="time2" name="timez">
                <label class="for-checkbox-budget" for="time2">Old</label>                      
                <input class="checkbox-budget" type="radio"  id="time5" value="time5" name="timez">
                <label class="for-checkbox-budget" for="time5"> Most Saved</label>                                        
            </div>  

            <button class="clean red-btn close-filter">Filter Result</button>
            
        
          
      </div>                 
 </div>
        
                
	</div>

</div>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script>
var d = new Date();
document.getElementById("year").innerHTML = d.getFullYear();
</script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>  
	
<script src="js/wow.min.js" type="text/javascript"></script>
    <script>
     new WOW().init();
    </script>	
<script src="js/headroom.js"></script>


<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
 <script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<script src="js/notification.js"></script>
<script src="js/notiflix-aio-1.9.1.js"></script>
<script src="js/notiflix-aio-1.9.1.min.js"></script>
<script src="js/rrpowered_notification_script.js"></script>

<!--- Modal Box --->
<script>

var statusmodal = document.getElementById("status-modal");
var companymodal = document.getElementById("company-modal");
var loginmodal = document.getElementById("login-modal");
var signupmodal = document.getElementById("signup-modal");
var forgotmodal = document.getElementById("forgot-modal");
var filtermodal = document.getElementById("filter-modal");
var filtercmodal = document.getElementById("filterc-modal");


var openstatus = document.getElementsByClassName("open-status")[0];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openforgot = document.getElementsByClassName("open-forgot")[0];
var openfilter = document.getElementsByClassName("open-filter")[0];
var openfilterc = document.getElementsByClassName("open-filterc")[0];

var closestatus = document.getElementsByClassName("close-status")[0];

var opencompany = document.getElementsByClassName("open-company")[0];

var closecompany = document.getElementsByClassName("close-company")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closefilter = document.getElementsByClassName("close-filter")[0];
var closefilter1 = document.getElementsByClassName("close-filter")[1];
var closefilterc = document.getElementsByClassName("close-filterc")[0];
var closefilterc1 = document.getElementsByClassName("close-filterc")[1];

if(openstatus){
openstatus.onclick = function() {
  statusmodal.style.display = "block";
}
}

if(closestatus){
closestatus.onclick = function() {
  statusmodal.style.display = "none";
}
}

if(opencompany){
  opencompany.onclick = function() {
  companymodal.style.display = "block";
}
}

if(closecompany){
  closecompany.onclick = function() {
  companymodal.style.display = "none";
}
}


if(openlogin){
openlogin.onclick = function() {


  loginmodal.style.display = "block";  
}
}
if(openlogin1){
openlogin1.onclick = function() {

  forgotmodal.style.display = "none";  	
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";  	
  loginmodal.style.display = "block";

}
}
if(opensignup){
opensignup.onclick = function() {
  loginmodal.style.display = "none";

  forgotmodal.style.display = "none";
  signupmodal.style.display = "block";  
}
}
if(opensignup1){
opensignup1.onclick = function() {
  loginmodal.style.display = "none";

  forgotmodal.style.display = "none";  
  signupmodal.style.display = "block";  
}
}
if(opensignup2){
opensignup2.onclick = function() {
  loginmodal.style.display = "none";

  forgotmodal.style.display = "none"; 
  signupmodal.style.display = "block";   
}
}
if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";  
  loginmodal.style.display = "none";

}
}
if(openfilter){
openfilter.onclick = function() {
  filtermodal.style.display = "block";  

}
}
if(openfilterc){
openfilterc.onclick = function() {
  filtercmodal.style.display = "block";  

}
}

if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closefilter){
closefilter.onclick = function() {
  filtermodal.style.display = "none";
}
}
if(closefilter1){
closefilter1.onclick = function() {
  filtermodal.style.display = "none";
}
}
if(closefilterc){
closefilterc.onclick = function() {
  filtercmodal.style.display = "none";
}
}
if(closefilterc1){
closefilterc1.onclick = function() {
  filtercmodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == statusmodal) {
    statusmodal.style.display = "none";
  }       

  if (event.target == companymodal) {
    companymodal.style.display = "none";
  }       

  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }       

  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }       

  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }       
  if (event.target == filtermodal) {
    filtermodal.style.display = "none";
  }     
  if (event.target == filtercmodal) {
    filtercmodal.style.display = "none";
  }    
  
}
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script> 

<!-- JS Notice Modal Start-->
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>
<!-- JS Notice Modal Start End-->
    <script src="js/glider.js"></script>
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider1').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider1').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider1').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider1').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider1'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider2').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider2').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider2').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider2').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider2'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>    
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider3').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider3').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider3').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider3').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider3'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>  
        <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider4').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider4').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider4').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider4').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider4'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>  
        <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider5').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider5').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider5').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider5').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider5'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>  
         <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider6').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider6').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider6').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider6').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider6'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>     
<script>
function copyID() {
  /* Get the text field */
  var copyText = document.getElementById("tokenID");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  navigator.clipboard.writeText(copyText.value);

}
</script>
