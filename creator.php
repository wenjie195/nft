
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="NFT Creators | The Laureate League" />
        <title>NFT Creators | The Laureate League</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding">
	<div class="width100 overflow nft-title-div">
    	<h1 class="nft-title black-text weight900">Creators</h1>
        <form class="search-form">
        	<input class="clean search-input" type="text" placeholder="Search">
            <img src="img/search.png" class="search-png" title="Search" alt="Search">
        </form>
        <!--Filter pop out at js.php --->
        <p class="filter-p red-link open-filterc"><span>Filter</span> <img src="img/filter.png" class="filter-png" alt="Filter" title="Filter"></p>
    </div></div>
    <div class="clear"></div>
    <div class="width10x  ow-sakura-height">
    		<!-- Repeat this -->
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic1.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Ele</p>
                <p class="width100 grey-desc text-overflow">3 Collections</p>
                <p class="right-fav text-overflow hover1 ow-width100"><span>10</span> <img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></p>
               
              </div>
            </div>
          </a>
			<!-- End of Repeat, can remove bottom -->		
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic4.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">
                <p class="width100 text-overflow slider-product-name">Rider</p>
                <p class="width100 grey-desc text-overflow">3 Collections</p>
                <p class="right-fav text-overflow hover1 ow-width100"><span>10</span> <img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></p>


              </div>
            </div>
          </a>
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic8.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">
                <p class="width100 text-overflow slider-product-name">Deer</p>
                <p class="width100 grey-desc text-overflow">2 Collections</p>
                <p class="right-fav text-overflow hover1 ow-width100"><span>10</span> <img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></p>
              </div>
            </div>
          </a>          

</div>


<?php include 'js.php'; ?>


</body>
</html>