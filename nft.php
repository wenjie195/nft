
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="NFT Market Place | The Laureate League" />
        <title>NFT Market Place | The Laureate League</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding">
	<div class="width100 overflow nft-title-div">
    	<h1 class="nft-title black-text weight900">NFT</h1>
        <form class="search-form">
        	<input class="clean search-input" type="text" placeholder="Search">
            <img src="img/search.png" class="search-png" title="Search" alt="Search">
        </form>
        <!--Filter pop out at js.php --->
        <p class="filter-p red-link open-filter"><span>Filter</span> <img src="img/filter.png" class="filter-png" alt="Filter" title="Filter"></p>
    </div></div>
    <div class="clear"></div>
    <div class="width10x  ow-sakura-height">
    		<!-- Repeat this -->
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic1.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Elephant#001</p>
                <p class="width100 grey-desc text-overflow">Ele</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>
			<!-- End of Repeat, can remove bottom -->		
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic2.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Elephant#001</p>
                <p class="width100 grey-desc text-overflow">Ele</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic3.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Elephant#001</p>
                <p class="width100 grey-desc text-overflow">Ele</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>          
      
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic4.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Horse#001</p>
                <p class="width100 grey-desc text-overflow">Rider</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>          
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic5.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Horse#002</p>
                <p class="width100 grey-desc text-overflow">Rider</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>           
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic6.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Horse#003</p>
                <p class="width100 grey-desc text-overflow">Rider</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>           
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic7.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Deer#001</p>
                <p class="width100 grey-desc text-overflow">Deer</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>           
          <a href='#'>
            <div class="shadow-white-box product-box hover-effect">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content">
                      
                        <img src="img/pic8.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Deer#002</p>
                <p class="width100 grey-desc text-overflow">Deer</p>
                <p class="width100 grey-desc text-overflow">2.5ETH</p>
                <div class="clear"></div>
                <p class="left-cond text-overflow">Last 2.3 ETH</p>
                <div class="right-fav text-overflow"><span>10</span> <form><button class="clean transparent border0  hover1 padding0"><img src="img/bookmark1.png" class="bookmark hover1a"><img src="img/bookmark2.png" class="bookmark hover1b"></button></form></div>
              </div>
            </div>
          </a>           
</div>


<?php include 'js.php'; ?>


</body>
</html>